# Rapport indépendant sur l'Écologie du Digital

> Écologie du Digital - État des lieux et démarches de sobriété

> Accent mis sur le Développement Web

### Objectif

Produire et partager un rapport sur l'impact écologique du numérique, en concentrant l'étude particulièrement sur web dont l'usage est en forte croissance.

Le rapport doit être le plus fiable possible, les données doivent être appuyées par des sources reconnues ou être produites par nos soins mais doivent ensuite être vérifiées.

## Autres liens GIT liés au projet

 - [impact-lecture-automatique-video](https://framagit.org/nicolaslienart/impact-lecture-automatique-video)
 - [navigation-web-analyse-du-trafic](https://framagit.org/nicolaslienart/navigation-web-analyse-du-trafic)
 - [trafic-de-donnees-cookies-pubs-pistage](https://framagit.org/nicolaslienart/trafic-de-donnees-cookies-pubs-pistage)

## Contributions

Le rapport et les moyens de mesure ont été mis en ligne à des fins de transparence, afin que la communauté puisse vérifier les chiffres et avancées ou adapter le moyen de mesure pour le rendre plus fiable.

Chacun est libre de contribuer, veillez à bien expliquer vos observations pour faciliter le dialogue svp.

## Auteur

Je suis Nicolas Liénart, étudiant dans le multimédia à l'école d'ingénieur IMAC (Univeristé Gustave Eiffel) et je mène actuellement ce projet de rapport de façon indépendante de toute société ou institution.

L'environnement et sa préservation me tiennent à coeur et je veux éviter que le numérique soit destructeur. Je veux donc aider les autres personnes qui font usage du numérique à identifier les problématiques écologiques et à adopter une démarche plus sobre et à mener leurs projets de façon respectueuse.
